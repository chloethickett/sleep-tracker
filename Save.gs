function save() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let overview_sheet = spreadsheet.getSheetByName(OVERVIEW_SHEET);

  let values_to_update = spreadsheet.getRange(`${INPUT_SHEET}!C2:C4`).getDisplayValues();
  let date_string = values_to_update[0][0];
  let value = values_to_update[1][0];
  let value_type = values_to_update[2][0];

  switch(value_type){
    case GO_TO_BED:
      var column = 3;
      break;
    case GET_UP:
      var column = 5;
      break;
    case MULTIVITAMIN:
      var column = 7;
      break;
    default:
      spreadsheet.getRange(`${INPUT_SHEET}!C4`).setValue('select a type!');
      return;
  }

  let save_date = stringToDate(date_string);

  let dates = spreadsheet.getRange(`${OVERVIEW_SHEET}!B3:B`).getDisplayValues();
  let check_date = checkDateExists(save_date, dates);
  let row = check_date[1];
  
  if (check_date[0]) {
    overview_sheet.getRange(row, column).setValue(value);
  } else {
    insertRow(row, date_string, spreadsheet, overview_sheet, column, value);
  }

  resetPointsInput(spreadsheet);
}

function resetPointsInput(spreadsheet) {
  spreadsheet = spreadsheet || SpreadsheetApp.getActiveSpreadsheet();
  spreadsheet.getRange(`${INPUT_SHEET}!C2:C4`).clear({contentsOnly: true});
}

function checkDateExists(date_to_compare, dates) {
  for (let i = dates.length - 1; i >= 0; i--) {
    var date = stringToDate(dates[i][0].toString());
    if (date === date_to_compare) {
      return [true, i + 3];
    } else if (date < date_to_compare) {
      return [false, i + 4];
    }
  }
  return [false, 3];
}

function stringToDate(date_string) {
  let date_split = date_string.split('/');
  let date = new Date(+date_split[2], +date_split[1] - 1, +date_split[0]).setHours(0,0,0,0);
  return date;
}

function insertRow(poRow, date_string, spreadsheet, overview_sheet, column, value) {
  let poValues = pointsOverviewRow(poRow, date_string);
  if (column) {
    poValues[0][column - 1] = value;
  }
  if (poRow == 3) {
    overview_sheet.insertRowBefore(poRow);
  } else {
    overview_sheet.insertRowAfter(poRow - 1);
  }
  overview_sheet.getRange(poRow, 1, 1, 8).setValues(poValues);

  let dpRow = poRow + 1;
  let dpValues = dailyPointsRow(dpRow);
  let daily_sheet = spreadsheet.getSheetByName(ALL_DATA_SHEET);
  if (dpRow > 11) {
    daily_sheet.insertRowAfter(dpRow - 1);
    daily_sheet.getRange(dpRow, 1, 1, 34).setValues(dpValues);
  } else {
    daily_sheet.insertRowBefore(4);
    daily_sheet.getRange(4, 1, 8, 34).setValues(dpValues);
  }
}

function pointsOverviewRow(row, date_string) {
  let values = [ 
    [
      `=$B${row}`, 
      date_string, 
      '', 
      `=IFS(C${row}="","", `
        + `C${row}<TIME(4,0,0), QUOTIENT((4-HOUR(C${row}))*(5-HOUR(C${row})),2) - QUOTIENT((4-HOUR(C${row}))*MINUTE(C${row}),60), `
        + `C${row}<TIME(18,0,0),0, `
        + `C${row}<TIME(22,0,0),22, `
        + `C${row}>=TIME(22,0,0), QUOTIENT((28-HOUR(C${row}))*(29-HOUR(C${row})),2) - QUOTIENT((28-HOUR(C${row}))*MINUTE(C${row}),60))`, 
      '', 
      `=IFS(E${row}="","", `
        + `E${row}<TIME(4,0,0),0, `
        + `E${row}<TIME(6,0,0),6, `
        + `E${row}<TIME(12,0,0),12 - HOUR(E${row}), `
        + `E${row}>=TIME(12,0,0),0)`,
      '0',
      `=IF($B${row}="", "", $D${row}+$F${row}+$G${row})`
    ]
  ];
  return values;
}

function dailyPointsRow(dpRow) {
  if (dpRow > 11) {
    return [oneDailyPointsRow(dpRow)];
  }
  let values = [
    oneDailyPointsRow(4),
    oneDailyPointsRow(5),
    oneDailyPointsRow(6),
    oneDailyPointsRow(7),
    oneDailyPointsRow(8),
    oneDailyPointsRow(9),
    oneDailyPointsRow(10),
    oneDailyPointsRow(11),
  ];
  return values;
}

function oneDailyPointsRow(dpRow) {
  let poRow = dpRow - 1;
  if (dpRow < 11) {
    var dpRow_week_offset = 4;
  } else {
    var dpRow_week_offset = dpRow - 6;
  }

  function totalString(letter1, letter2) {return `=IF($A${dpRow}="", "", $${letter1}${dpRow}+$${letter2}${dpRow})`}
  function differenceString(letter1, letter2) {return `=IF($A${dpRow}="", "", $${letter1}${dpRow}-$${letter2}${dpRow})`}
  function averageString(letter) {return `=IF($A${dpRow}="", "",AVERAGE(${letter}$4:${letter}${dpRow}))`}
  function medianString(letter) {return `=IF($A${dpRow}="", "",MEDIAN(${letter}$4:${letter}${dpRow}))`}
  function weeklyAverageString(letter){return `=IF($A${dpRow}="", "",AVERAGE(${letter}${dpRow_week_offset}:${letter}${dpRow}))`}
  function weightedWeeklyAverageString(letter1, letter2) {
    if (dpRow > 10) {
      var string_end = `,${letter2}${dpRow - 7}`;
    } else {
      var string_end =  ``;
    }
    return `=IF($A${dpRow}="", "",AVERAGE(${letter1}${dpRow_week_offset}:${letter1}${dpRow}${string_end}))`
  }
  function cumulativeTotalString(letter1, letter2) {
    if (dpRow > 4) {
      var string_end = `+$${letter2}${dpRow - 1}`;
    } else {
      var string_end =  ``;
    }
    return `=IF($A${dpRow}="", "", $${letter1}${dpRow}${string_end})`
  }
  function straightFromOverview(letter, check_empty){
    var base = `'${OVERVIEW_SHEET}'!$${letter}${poRow}`
    if (check_empty) {
      return `=IF($A${dpRow}="", "",${base})`
    } else {
      return `=${base}`
    }
  }

  let values = [
    straightFromOverview('B'),
    `=IF($A${dpRow}="", "",IF('${OVERVIEW_SHEET}'!$C${poRow} < TIME(18,0,0), '${OVERVIEW_SHEET}'!$C${poRow}, '${OVERVIEW_SHEET}'!$C${poRow}-1))`,
    weeklyAverageString('B'),
    straightFromOverview('E', true),
    weeklyAverageString('D'),
    straightFromOverview('D'),
    straightFromOverview('F'),
    totalString('F','G'),
    straightFromOverview('G', true),
    totalString('H', 'I'),
    cumulativeTotalString('J', 'K'),
    weeklyAverageString('F'),
    weeklyAverageString('G'),
    weeklyAverageString('H'),
    weeklyAverageString('I'),
    weeklyAverageString('J'),
    averageString('F'),
    averageString('G'),
    averageString('H'),
    averageString('I'),
    averageString('J'),
    medianString('F'),
    medianString('G'),
    medianString('H'),
    medianString('I'),
    medianString('J'),
    weightedWeeklyAverageString('F', 'AA'),
    weightedWeeklyAverageString('G', 'AB'),
    weightedWeeklyAverageString('H', 'AC'),
    weightedWeeklyAverageString('I', 'AD'),
    weightedWeeklyAverageString('J', 'AE'),
    `=IF($A${dpRow}="", "", SUMIF(${REWARDS_SHEET}!F:F, $A${dpRow}, ${REWARDS_SHEET}!D:D))`,
    differenceString('J', 'AF'),
    cumulativeTotalString('AG', 'AH')
  ];
  return values;
}

