function onEdit(e) {
  if (e.range.getA1Notation() == 'C6' && e.range.getSheet().getSheetName() == 'Points Input') {
    if (/^\w+$/.test(e.value)) {        
      eval(e.value)();
      e.range.clear({contentsOnly: true});
    }
  }
}
