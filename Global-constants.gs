// Input types
let GO_TO_BED = 'Go to bed';
let GET_UP = 'Get up';
let MULTIVITAMIN = 'Multivitamin';

// Date and time layouts
let DATE_FORMAT = 'dd/MM/yyyy';
let TIME_FORMAT = 'HH:mm';

// Sheet names
let OVERVIEW_SHEET = 'Points Overview';
let INPUT_SHEET = 'Points Input';
let REWARDS_SHEET = 'Rewards';
let ALL_DATA_SHEET = 'Daily Points';
