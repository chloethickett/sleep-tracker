function goToBed() {
  let datetime = new Date();
  let sheet = SpreadsheetApp.getActiveSheet();
  let time_zone = setTimeZone(sheet);
  let time = Utilities.formatDate(datetime, time_zone, TIME_FORMAT);
  if (time >= '18:00') {
    datetime.setDate(datetime.getDate() + 1);
  }
  let date = Utilities.formatDate(datetime, time_zone, DATE_FORMAT);
  setDateValueType(date, time, GO_TO_BED,sheet);
}

function getUp() {
  let datetime = new Date();
  let sheet = SpreadsheetApp.getActiveSheet();
  let time_zone = setTimeZone(sheet);
  let date = Utilities.formatDate(datetime, time_zone, DATE_FORMAT);
  let time = Utilities.formatDate(datetime, time_zone, TIME_FORMAT);
  setDateValueType(date, time, GET_UP, sheet);
}

function multivitamin() {
  let sheet = SpreadsheetApp.getActiveSheet();
  let time_zone = setTimeZone(sheet);
  let date = Utilities.formatDate(new Date(), time_zone, DATE_FORMAT);
  setDateValueType(date, 1, MULTIVITAMIN, sheet);
}

function setDateValueType(date, value, type, sheet) {
  sheet = sheet || SpreadsheetApp.getActiveSheet();
  let values = [ [date], [value], [type] ];
  sheet.getRange('C2:C4').setValues(values);
}

function setTimeZone(sheet) {
   sheet = sheet || SpreadsheetApp.getActiveSheet();
   let time_zone = sheet.getRange('A1').getValue();
   return time_zone;
}
