function displayLastSleepEntry() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let times = spreadsheet.getRange(`${OVERVIEW_SHEET}!B3:E`).getDisplayValues();
  setDateValueType.apply(null, checkLastSleepEntry(times, check_get_up = true, check_go_to_bed = true))
}

function displayLastGoToBedEntry() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let times = spreadsheet.getRange(`${OVERVIEW_SHEET}!B3:E`).getDisplayValues();
  setDateValueType.apply(null, checkLastSleepEntry(times, check_get_up = false, check_go_to_bed = true))
}

function displayLastGetUpEntry() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let times = spreadsheet.getRange(`${OVERVIEW_SHEET}!B3:E`).getDisplayValues();
  setDateValueType.apply(null, checkLastSleepEntry(times, check_get_up = true, check_go_to_bed = false))
}

function checkLastSleepEntry(times, check_get_up, check_go_to_bed) {
  for (let i = times.length - 1; i >= 0; i--) {
    let date = times[i][0];
    let go_to_bed_time = times[i][1];
    let get_up_time = times[i][3];
    if (check_get_up && get_up_time != "") {
      return [date, get_up_time, GET_UP];
    }
    if (check_go_to_bed && go_to_bed_time != "") {
      return [date, go_to_bed_time, GO_TO_BED];
    }
  }
  return ["", "No Entries Found", ""]
}
